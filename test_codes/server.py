import asyncio
import i2plib


async def accept_test():
    session_name = "test-server"

    # generate server destination
    server_destination = await i2plib.new_destination(sig_type=11)
    print(f'server b32 address: {server_destination.base32}.b32.i2p')
    print(f'server bb32 address: {server_destination.bb32()}.b32.i2p')

    # create a SAM stream session
    await i2plib.create_session(session_name, destination=server_destination, options={'i2cp.leaseSetType': '5'})

    # accept a connection
    reader, writer = await i2plib.stream_accept(session_name)
    print('server ready')

    # first string on a client connection always contains clients I2P destination
    dest = await reader.readline()
    remote_destination = i2plib.Destination(dest.decode().strip())
    print(f'accepted connection from {remote_destination.base32}.b32.i2p')

    while True:
        writer.write(bytes(input('message to client: '), 'utf-8'))
        data = await reader.read(4096)
        print(f'message from client: {data.decode()}')

    # close the connection
    writer.close()

# run event loop
loop = asyncio.get_event_loop()
loop.run_until_complete(accept_test())
loop.stop()
