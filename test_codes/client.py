import asyncio
import i2plib


async def connect_test():
    session_name = "test-client"

    dest_inp = input('please input .b32.i2p address to connect: ')

    # destination = await i2plib.dest_lookup(dest_inp)

    # create a SAM stream session
    await i2plib.create_session(session_name)

    # connect to a destination
    reader, writer = await i2plib.stream_connect(session_name, dest_inp)
    print('client connected to server')

    while True:
        writer.write(bytes(input('message to server: '), 'utf-8'))
        data = await reader.read(4096)
        print(f'message from server: {data.decode()}')


    # close the connection
    writer.close()

# run event loop
loop = asyncio.get_event_loop()
loop.run_until_complete(connect_test())
loop.stop()
