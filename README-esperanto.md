# bchat-i2p

[English](https://codeberg.org/weko/bchat-i2p/src/branch/main/README.md)

### Ĉi tiu programo estas distribuita sub libera permesilo [GNU Ĝenerala Publika Permesilo versio 3](https://www.gnu.org/licenses/gpl-3.0.en.html) kaj supre.

### ⚠️ Atentu: projekto pri testado kaj disvolva etapo. Ne uzu ĉi tiun kodon se vi ne komprenas, kion vi faras.

## Priskribo
i2p-bazita babilejo en Python, uzata de SAM por komuniki kun la enkursigilo. Por SAM, la forko de i2plib-biblioteko estas uzata.

### Terminologio de projekto
- **Kliento** - programo por vidi kaj sendi mesaĝojn, krei adresojn kaj transdoni al la servilo
- **Servilo** - programo por stoki, ricevi mesaĝojn kaj krei adresojn por komunikado ene de la reto
    - Serviloj estas rekte implikitaj en la interŝanĝo de mesaĝoj.
- **Profiloj** - unikaj personoj, diversaj profiloj ne intersekcas inter si
    - Ĉiu profilo havas ĉefajn servilojn kaj klientajn i2p-adresojn por konekti kun ĉi tiu profilo
- **Kontaktoj** - foraj profiloj, kun sorĉistino loka profilo povas komuniki
    - Sur ĉiu kontaktoprofilo havas unikajn klientajn kaj servilojn i2p-adresojn
    - Por kontakti foran profilon, vi devas skribi la ĉefan adreson de fora profilo, tiam bchat-i2p generi adresojn por ĉi tiu kontakto
   
### Uzu variantojn
- kliento + servilo (ununura aparato) - komunikado loke per funkciovokoj
- kliento + fora servilo (klienta aparato kaj fidinda servilo) - komunikado inter kliento kaj servilo per i2p
- **eble** la uzo de volontulaj serviloj (kun taŭga sekureco) estos efektivigita.


## Utilaj ligiloj
[I2P-ejo](https://geti2p.net) | [i2pd-dokumentoj](https://i2pd.readthedocs.io/en/latest/) | [i2plib originala deponejo](https://github.com/l-n-s/i2plib) | [i2plib-fork-deponejo](https://codeberg.org/weko/i2plib-fork) | [i2plib-dokumentoj](https://i2plib.readthedocs.io/en/latest/)

