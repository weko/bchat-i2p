# bchat-i2p

[Esperanto](https://codeberg.org/weko/bchat-i2p/src/branch/main/README-esperanto.md)

### This program is distributed under a free license [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html) and above.

### ⚠️ Attention: project on testing and development stage. Do not use this code if you do not understand what you are doing.

## Description
i2p-based chat in Python, used by SAM to communicate with the router. For SAM, the fork of i2plib library is used.

### Terminology of project
- **Client** - a program for viewing and sending messages, creating addresses and transferring to the server 
- **Server** - a program for storing, receiving messages and creating addresses for communication within the network 
    - Servers are directly involved in the exchange of messages.
- **Profiles** - unique persons, various profiles don't intersect between themselves 
    - Each profile has main server and client i2p addresses to connect with this profile 
- **Contacts** - remote profiles, with witch local profile can communicate 
    - On each contact profile has unique client and server i2p addresses 
    - To contact remote profile, you need to write remote profile's main address, then bchat-i2p generate addresses for this contact
   
### Use variants
- client + server (single device) - communication locally via function calls 
- client + remote server (client device and trusted server) - communication between client and server via i2p 
- **perhaps** the use of volunteer servers (with appropriate security) will be implemented.


## Useful links
[I2P site](https://geti2p.net) | [i2pd docs](https://i2pd.readthedocs.io/en/latest/) | [i2plib original repository](https://github.com/l-n-s/i2plib) | [i2plib fork repository](https://codeberg.org/weko/i2plib-fork) | [i2plib docs](https://i2plib.readthedocs.io/en/latest/)
