# bchat-i2p: P2P, I2P based, anonymous and private chat
# Copyright (C) 2022 weko
#
# This file is a part of bchat-i2p.
#
# bchat-i2p is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import aiosqlite

db = await aiosqlite.connect('server_db.db')

profiles_req = """CREATE TABLE IF NOT EXISTS profile (
    id INT PRIMARY KEY AUTOINCRIMENT NOT_NULL,
    local_name TEXT NOT_NULL,
    public_name TEXT NOT_NULL),
    main_pub_key TEXT,
    main_priv_key TEXT;

"""

contacts_req = """CREATE TABLE IF NOT EXISTS contact (
    id INT PRIMARY KEY AUTOINCRIMENT NOT_NULL,
    FOREIGN KEY (profile) REFERENCES profile(id),
    name TEXT,
    local_name TEXT,
    local_server_pub_key TEXT,
    local_server_priv_key TEXT,
    local_client_pub_key TEXT,
    local_client_priv_key TEXT,
    remote_server_pub_key TEXT,
    remote_client_pub_key TEXT);
"""

messages_req = """CREATE TABLE IF NOT EXISTS message (
    id INT PRIMARY KEY AUTOINCRIMENT NOT_NULL,
    FOREIGN KEY (contact) REFERENCES contact(id),
    from TEXT NOT_NULL,
    message_number INT NOT_NULL,
    remote_timestamp INT NOT_NULL,
    local_timestamp INT NOT_NULL,
    text TEXT);
"""
